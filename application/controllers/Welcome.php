<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API = "http://5.199.133.130:9001" . '/articles';
	}
    
    // menampilkan data kontak
    function index(){
		$data['list'] = json_decode($this->curl->simple_get($this->API));
		$this->load->view('all_article', $data);
    }
}
