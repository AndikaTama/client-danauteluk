<?php 
/**
* Author Andika Ganteng
*/
class Article extends CI_Controller
{
    function __construct() {
        parent::__construct();
	}
    
    // menampilkan data kontak
    function index(){
		
    }

	function show($value='')
	{
		$data['list'] = json_decode($this->curl->simple_get("http://5.199.133.130:9001/articles/" . $value));
		$this->load->view('article', $data);
	}

	function search(){
		$field = $_GET['field'];
		$keyword = $_GET['keyword'];
		if ($field == 'judul') {
			$data['list'] = json_decode($this->curl->simple_get("http://5.199.133.130:9001/articles/judul/" . $keyword));
		} else if ($field == 'tanggal') {
			$data['list'] = json_decode($this->curl->simple_get("http://5.199.133.130:9001/articles/tgl/" . $keyword));
		} else {
			$data['list'] = json_decode($this->curl->simple_get("http://5.199.133.130:9001/article?" . $field . "=" . $keyword));
		}
		
		$this->load->view('all_article', $data);
	}
}
 ?>