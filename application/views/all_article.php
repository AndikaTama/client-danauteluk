
<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet prefetch' href="<?= base_url() ?>assets/css/style.css">
<script src="<?= base_url() ?>assets/js/script.js"></script>
<title>DEMO WEBSITE PEMERINTAH KOTA JAMBI</title>
<style type="text/css">
    #halaman { width:2000x; margin:0 auto; }
    #header { height:auto; }
    #kiri { height:auto; padding:10px; float:left; margin: 15px; }
    #kanan { height:auto; padding-right:100px; float:right; margin-top:10px; }
    #tengah { height:auto;margin:10px 425px 0 325px; text-align:center;}
    #footer { height:auto;padding:10px; margin-top:10px;text-align:center;}
</style>
</head>
<body>
<div class="menu-wrap">
<ul>
    <li> 
      <a href="beranda.html"><img width="200px" height="60px" src="<?= base_url() ?>assets/image/home.jpg"></a>
    <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Dokumen Daerah</a></li>
            <li><a href="#">Aplikasi Daerah</a></li>
        </ul>
    </li>
    <li style="margin-left: 30px;"><a href="#">SEKILAS KOTA JAMBI</a></li>
    <li><a href="#">INFO PERIZINAN</a></li>
    <li><a href="#">PAJAK</a></li>
    <form method="GET" action="<?= base_url() ?>index.php/article/search">
          <select name="field" id="by">
            <option value="id">id</option>
            <option value="judul">judul</option>
            <option value="status">status</option>
            <option value="tanggal">tanggal</option>
          </select>
					<input type="text" class="searchtext" name="keyword" placeholder="search....">
					<input type="submit" class="searchsubmit" name="submit" value="search">
				</form>
</ul>
</div>
<div id="halaman">
<div id="header"><img src="<?= base_url() ?>assets/image/header.png" alt=""></div>

 <div id="kiri">
  <img src="<?= base_url() ?>assets/image/kiri-news.jpg" alt="">

</div>
 <!-- ======================================== BAGIAN KANAN ======================================  -->
<div id="kanan">
  <img src="assets/image/kanan-news.jpg" alt="">

</div>

<!-- =================================== Web service ========================================= -->
<div id="tengah">

  <section id="articles">
  
    <?php 
    if ( ($list != null) && ($list->data != null)) {
    foreach ($list->data as $key => $value) { ?>
    <article class="module">
      <div class="inside-module">
        <div class="bar-full">
          <span class="bar"></span>
          <div class="bar-box">
            <span class="bar-flip"><?= $value->judul; ?></span>
          </div> 
        </div>
    
        <div class="pad">
          <h2><?= $value->judul; ?></h2>
          <?= $value->tgl; ?> | <?= $value->jenis; ?> | <?= $value->dibaca; ?> | <?= $value->sumber; ?> <br><br>
          <p><?= $value->isi; ?></p>
          <p> <a href="<?= base_url() ?>index.php/article/show/<?= $value->id; ?>"> <b>Read More</b></a> </p>
        </div>
      </div>
    
    </article>
    
    <?php } 
    } else { echo "Data Tidak Ditemukan"; } 
    ?>
    
  </section>

</div>

<div id="footer">
<img src="<?= base_url() ?>assets/image/sembako.jpg" alt="">
<br>
    <img src="<?= base_url() ?>assets/image/spanduk-footer.jpg" alt="">
		  <p>Copyright &copy; 2017. Pemerintah Kota Jambi</a></p>
				</div>
</div>
</body>
</html>