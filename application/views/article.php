
<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet prefetch' href="<?= base_url() ?>assets/css/style.css">
<script src="<?= base_url() ?>assets/js/script.js"></script>
<title>DEMO WEBSITE PEMERINTAH KOTA JAMBI</title>
<style type="text/css">
    #halaman { width:2000x; margin:0 auto; }
    #header { height:auto; }
    #kiri { height:auto; padding:10px; float:left; margin: 15px; }
    #kanan { height:auto; padding-right:100px; float:right; margin-top:10px; }
    #tengah { height:auto;margin:10px 425px 0 325px; text-align:center;}
    #footer { height:auto;padding:10px; margin-top:10px;text-align:center;}
</style>
</head>
<body>
<div class="menu-wrap">
<ul>
<li> 
  <a href="beranda.html"><img width="200px" height="60px" src="<?= base_url() ?>assets/image/home.jpg"></a>
<ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Dokumen Daerah</a></li>
            <li><a href="#">Aplikasi Daerah</a></li>
        </ul>
    </li>
    <li><a href="#">SEKILAS KOTA JAMBI</a></li>
    <li><a href="#">INFO PERIZINAN</a></li>
    <li><a href="#">PAJAK</a></li>
    <form>
					<input type="text" class="searchtext" name="search" placeholder="search....">
					<input type="submit" class="searchsubmit" name="submit" value="search">
				</form>
</ul>
</div>
<!-- =================================== Header ========================================= -->
<div id="halaman">
<div id="header"><img src="<?= base_url() ?>assets/image/header.png" alt=""></div>

<div id="kiri">
  <img src="<?= base_url() ?>assets/image/kiri-news.jpg" alt="">

</div>
<!-- ======================================== BAGIAN KANAN ====================================== -->
<div id="kanan">
  <img src="<?= base_url() ?>assets/image/kanan-news.jpg" alt="">

</div>

<!-- =================================== Web service ========================================= -->
<div id="tengah">
<section id="articles">
 

  <?php 
  if ( ($list != NULL) && ($list->data != NULL)) {
      $get = $list->data[0];
    ?>
  <article class="module">
    <div class="inside-module">
      <div class="bar-full">
        <span class="bar"></span>
        <div class="bar-box">
          <span class="bar-flip"><?= $get->judul; ?></span>
        </div> 
      </div>
  
      <div class="pad">
        <h2><?= $get->judul; ?></h2>
        <?= $get->tgl; ?> | <?= $get->jenis; ?> | <?= $get->dibaca; ?> | <?= $get->sumber; ?> <br><br>
        <p><?= $get->isi; ?></p>
      </div>
    </div>
  
  </article>
  
  <?php }
  else { echo "Data Tidak Ditemukan"; } ?>
  
  <script src="<?= base_url() ?>assets/js/script.js"></script>
</section>
</div>



<div id="footer">
<img src="<?= base_url() ?>assets/image/sembako.jpg" alt="">
<br>
    <img src="<?= base_url() ?>assets/image/spanduk-footer.jpg" alt="">
		  <p>Copyright &copy; 2017. Pemerintahan Kota Jambi</a></p>
				</div>
</div>
</body></html>